//
//  AppDelegate.h
//  2048
//
//  Created by Admin on 2/2/17.
//  Copyright © 2017 RyanAbner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

