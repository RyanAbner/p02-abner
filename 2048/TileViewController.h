//
//  TileViewController.h
//  p02-abner
//
//  Created by Admin on 2/9/17.
//  Copyright © 2017 RyanAbner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TileViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *valueLabel;

- (void)setText:(NSString*) str;

@end
