//
//  ViewController.h
//  2048
//
//  Created by Admin on 2/2/17.
//  Copyright © 2017 RyanAbner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet UILabel *l00,*l01,*l02,*l03,
            *l10,*l11,*l12,*l13,
            *l20,*l21,*l22,*l23,
            *l30,*l31,*l32,*l33;

@property(nonatomic, strong) IBOutlet UILabel *scoreLabel;
@property(atomic, readwrite) int score;
@property(nonatomic, strong) IBOutlet UILabel *gameOverLabel;
@property (nonatomic,retain) AVAudioPlayer *songPlayer;

//Grid of references to tiles
@property (nonatomic, strong) NSArray *grid;

//List of unoccupied tiles
@property (nonatomic, strong) NSMutableArray *unoccupied;

@end


/* Design plan
 
Note: Have one NSSet for occupied tiles and one NSSet for unoccupied tiles
 
DONE - Initialize all tiles to blank
DONE - Figure out how initial tiles are set (a 2 or 4 in random places, maybe weighted toward 2) and initialize the grid with that
Figure out how to make tiles move - use UI buttons at first
 - Use the grid for this, changing labels - shouldn't be complicated
 - Probably easily extensible to images
Figure out how to merge tiles and increase score, implement that
    - If two sliding in one direction would cause two of the same tile to be adjacent on that plane (i.e. vertical or horizontal) they get merged in the direction you swiped
Figure out how other tiles are spawned (probably a 2 or 4 in random empty tile, weighted toward 2) and implement that
Show game over screen w/ option to restart when the grid is full with no more valid moves
    - How do you determine there are no more valid moves? Maybe test a left/right/up/down swipe and see if anything would merge, or look for adjacent tiles of same number
Make start screen
 
*/
