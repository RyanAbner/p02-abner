//
//  ViewController.m
//  2048
//
//  Created by Admin on 2/2/17.
//  Copyright © 2017 RyanAbner. All rights reserved.
//
//  Parts of this are copied from this tutorial: http://codewithchris.com/avaudioplayer-tutorial/


#import "ViewController.h"
#include <stdlib.h>

@interface ViewController ()
@end

@implementation ViewController
@synthesize l00,l01,l02,l03,
l10,l11,l12,l13,
l20,l21,l22,l23,
l30,l31,l32,l33;
@synthesize grid;
@synthesize unoccupied;
@synthesize scoreLabel;
@synthesize score;
@synthesize gameOverLabel;
@synthesize songPlayer;

#define ROW_SIZE 4
#define COL_SIZE 4
//Generates a 2 or 4 tile in a random unoccupied spot on the grid
//If this is called when all tiles are occupied, it gets all screwy. Don't  do that.
- (void)generateTile {
    int indexToUse = arc4random_uniform([unoccupied count]);
    //Gives a 3/4 chance of generating a 2, 1/4 chance of generating a 4.
    [unoccupied[indexToUse] setText:(arc4random_uniform(3) < 3) ? @"2" : @"4"];
    
    //Remove that index from unoccupied array
    [unoccupied removeObjectAtIndex:indexToUse];
    
    
}

//Returns true if there are any possible slides or merges left on the board.
- (bool)mergesPossible {
    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            if (i-1 >= 0){
                UILabel* curTile = grid[i][j];
                UILabel* adjTile = grid[i-1][j];
                if ([curTile.text isEqual:adjTile.text]){
                    //Two adjacent tiles are equal, so a merge is possible!
                    return true;
                }
            }
            if (j-1 >= 0){
                UILabel* curTile = grid[i][j];
                UILabel* adjTile = grid[i][j-1];
                if ([curTile.text isEqual:adjTile.text]){
                    //Two adjacent tiles are equal, so a merge is possible!
                    return true;
                }
            }
            if (i+1 < 4){
                UILabel* curTile = grid[i][j];
                UILabel* adjTile = grid[i+1][j];
                if ([curTile.text isEqual:adjTile.text]){
                    //Two adjacent tiles are equal, so a merge is possible!
                    return true;
                }
            }
            if (j+1 < 4){
                UILabel* curTile = grid[i][j];
                UILabel* adjTile = grid[i][j+1];
                if ([curTile.text isEqual:adjTile.text]){
                    //Two adjacent tiles are equal, so a merge is possible!
                    return true;
                }
            }
        }
    }
    return false;
}

//Empties game board, sets score to 0, generates two tiles
- (IBAction)reset {
    for (NSArray *line in grid){
        for (UILabel *elem in line){
            [elem setText:@""];
            if (![unoccupied containsObject:elem]){
                [unoccupied addObject:elem];
            }
        }
    }
    score = 0;
    [scoreLabel setText:@"0"];
    [gameOverLabel setText:@""];
    [self generateTile];
    [self generateTile];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"In viewDidLoad");
    [gameOverLabel setText:@""];
    //Do any additional setup after loading the view, typically from a nib.
    grid = @[@[l00,l01,l02,l03],
             @[l10,l11,l12,l13],
             @[l20,l21,l22,l23],
             @[l30,l31,l32,l33]];
    unoccupied = [NSMutableArray array];
    
    
    //Initialize grid to be empty and initialize unoccupied set with all tiles
    for (NSArray *line in grid){
        for (UILabel *elem in line){
            [elem setText:@""];
            [unoccupied addObject:elem];
        }
    }
    
    //Create two random starting tiles
    [self generateTile];
    [self generateTile];
    
    // Construct URL to sound file
    NSString *path = [NSString stringWithFormat:@"%@/2048 beatz.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *url = [NSURL fileURLWithPath:path];
    // Create audio player object and initialize with URL to sound
    songPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
    [songPlayer setNumberOfLoops:-1];//loop song forever
    [songPlayer play];
    NSLog(@"Past viewDidLoad\n");
}


//Slides tiles when button is pressed.
- (IBAction) slideUp {
    NSLog(@"In slideUp");
    //Move all occupied tiles up as far as they'll go
    //  Starting from top left corner and going left, then to next row and so on:
    //  if tile is occupied: check if tile above it is unoccupied. if it is, move it up to that tile. then check the tile above that one and so on.
    for (int i = 1; i < COL_SIZE; i++){//start at 2nd row because you'll never move the top row by sliding up
        for (int j = 0; j < ROW_SIZE; j++){
            NSLog(@"i=%@ j=%@\n",@(i),@(j));
            UILabel* currentTile = grid[i][j];
            if (![currentTile.text isEqual:@""]) {//If occupied
                //Want to move current tile up as many times as possible before hitting an occupied tile or the top
                
                int k = i-1;//highest unoccupied reachable tile - will get currentTile's label
                for (k = i-1; k > -1; k--){
                    UILabel* aboveTile = grid[k][j];
                    if (![aboveTile.text isEqual:@""]){
                        break;
                    }
                }
                k++;//k is now the row of the column to do the thing with
                if (i != k){
                    UILabel* tileToSet = grid[k][j];
                    [tileToSet setText:currentTile.text];
                    [currentTile setText:@""];
                    [unoccupied removeObject:tileToSet];
                    [unoccupied addObject:currentTile];
                }
                //If the tile now below the current one is equal, merge them downward
                if (k > 0){
                    UILabel* currentTile = grid[k][j];
                    UILabel* aboveTile = grid[k-1][j];
                    if ([currentTile.text isEqual:aboveTile.text]){
                        //make currentTile unoccupied and double value of belowTile
                        [currentTile setText:@""];
                        [unoccupied addObject:currentTile];
                        if ([aboveTile.text isEqual:@"2"]){
                            [aboveTile setText:@"4"];
                            score += 4;
                        } else if ([aboveTile.text isEqual:@"4"]){
                            [aboveTile setText:@"8"];
                            score += 8;
                        } else if ([aboveTile.text isEqual:@"8"]){
                            [aboveTile setText:@"16"];
                            score += 16;
                        } else if ([aboveTile.text isEqual:@"16"]){
                            [aboveTile setText:@"32"];
                            score += 32;
                        } else if ([aboveTile.text isEqual:@"32"]){
                            [aboveTile setText:@"64"];
                            score += 64;
                        }else if ([aboveTile.text isEqual:@"64"]){
                            [aboveTile setText:@"128"];
                            score += 128;
                        }else if ([aboveTile.text isEqual:@"128"]){
                            [aboveTile setText:@"256"];
                            score += 256;
                        }else if ([aboveTile.text isEqual:@"256"]){
                            [aboveTile setText:@"512"];
                            score += 512;
                        }else if ([aboveTile.text isEqual:@"512"]){
                            [aboveTile setText:@"1024"];
                            score += 1024;
                        }else if ([aboveTile.text isEqual:@"1024"]){
                            [aboveTile setText:@"2048"];
                            score += 2048;//TODO: Make it so that you win when you get a 2048 tile!
                        }
                        [scoreLabel setText:[@(score) stringValue]];
                    }
                }
            }
        }
    }
    
    //Call generateTile if the board isn't full
    if ([unoccupied count] > 0) {
        [self generateTile];
    }
    else {
        //Check if any more merges are possible. If not, game over!
        if (![self mergesPossible]){
            //Do game over stuff
            [gameOverLabel setText:@"Game over!"];
        }
    }
}

- (IBAction) slideRight{
    NSLog(@"In slideRight");
    //Move all occupied tiles right as far as they'll go
    //  Starting from top right corner and going down, then to next column and so on:
    //  if tile is occupied: check if tile right of it is unoccupied. if it is, move it right to that tile. then check the tile right of that one and so on.
    for (int i = ROW_SIZE-2; i >= 0; i--){//start at 2nd row because you'll never move the top row by sliding up
        for (int j = 0; j < COL_SIZE; j++){
            //i is column (x coord), j is row (y coord)
            UILabel* currentTile = grid[j][i];
            if (![currentTile.text isEqual:@""]) {//If occupied
                //Want to move current tile right as many times as possible before hitting an occupied tile or the right side
                
                int k;//will become the rightest unoccupied reachable tile
                for (k=i+1; k < 4; k++){
                    UILabel* rightTile = grid[j][k];
                    if (![rightTile.text isEqual:@""]){
                        break;
                    }
                }
                k--;
                NSLog(@"i=%@ j=%@ k=%@\n",@(i),@(j),@(k));
                NSLog(@"Current tile has value %@\n",currentTile.text);
                if (i != k){//if the tile will actually move
                    UILabel* tileToSet = grid[j][k];
                    [tileToSet setText:currentTile.text];
                    [currentTile setText:@""];
                    [unoccupied removeObject:tileToSet];
                    [unoccupied addObject:currentTile];
                }
                //If the tile now above the current one is equal, merge them upward
                if (k < 3){
                    UILabel* currentTile = grid[j][k];
                    UILabel* rightTile = grid[j][k+1];
                    if ([currentTile.text isEqual:rightTile.text]){
                        //make currentTile unoccupied and double value of rightTile
                        [currentTile setText:@""];
                        [unoccupied addObject:currentTile];
                        if ([rightTile.text isEqual:@"2"]){
                            [rightTile setText:@"4"];
                            score += 4;
                        } else if ([rightTile.text isEqual:@"4"]){
                            [rightTile setText:@"8"];
                            score += 8;
                        } else if ([rightTile.text isEqual:@"8"]){
                            [rightTile setText:@"16"];
                            score += 16;
                        } else if ([rightTile.text isEqual:@"16"]){
                            [rightTile setText:@"32"];
                            score += 32;
                        } else if ([rightTile.text isEqual:@"32"]){
                            [rightTile setText:@"64"];
                            score += 64;
                        }else if ([rightTile.text isEqual:@"64"]){
                            [rightTile setText:@"128"];
                            score += 128;
                        }else if ([rightTile.text isEqual:@"128"]){
                            [rightTile setText:@"256"];
                            score += 256;
                        }else if ([rightTile.text isEqual:@"256"]){
                            [rightTile setText:@"512"];
                            score += 512;
                        }else if ([rightTile.text isEqual:@"512"]){
                            [rightTile setText:@"1024"];
                            score += 1024;
                        }else if ([rightTile.text isEqual:@"1024"]){
                            [rightTile setText:@"2048"];
                            score += 2048;//TODO: Make it so that you win when you get a 2048 tile!
                        }
                        [scoreLabel setText:[@(score) stringValue]];
                    }
                }
            }
        }
    }
    
    //Call generateTile if the board isn't full
    if ([unoccupied count] > 0) {
        [self generateTile];
    }
    else {
        //Check if any more merges are possible. If not, game over!
        if (![self mergesPossible]){
            //Do game over stuff
            [gameOverLabel setText:@"Game over!"];
        }
    }
}

- (IBAction) slideDown {
    NSLog(@"In slideDown");
    //Move all occupied tiles up as far as they'll go
    //  Starting from top left corner and going left, then to next row and so on:
    //  if tile is occupied: check if tile above it is unoccupied. if it is, move it up to that tile. then check the tile above that one and so on.
    for (int i = COL_SIZE-2; i >= 0; i--){//start at 2nd row because you'll never move the top row by sliding up
        for (int j = 0; j < ROW_SIZE; j++){
            NSLog(@"i=%@ j=%@\n",@(i),@(j));
            UILabel* currentTile = grid[i][j];
            if (![currentTile.text isEqual:@""]) {//If occupied
                //Want to move current tile down as many times as possible before hitting an occupied tile or the bttom
                
                int k;//will become the row lowest unoccupied reachable tile
                for (k=i+1; k < 4; k++){
                    UILabel* belowTile = grid[k][j];
                    if (![belowTile.text isEqual:@""]){
                        break;
                    }
                }
                k--;
                if (i != k){//if the tile will actually move
                    UILabel* tileToSet = grid[k][j];
                    [tileToSet setText:currentTile.text];
                    [currentTile setText:@""];
                    [unoccupied removeObject:tileToSet];
                    [unoccupied addObject:currentTile];
                }
                //If the tile now above the current one is equal, merge them upward
                if (k < 3){
                    UILabel* currentTile = grid[k][j];
                    UILabel* belowTile = grid[k+1][j];
                    if ([currentTile.text isEqual:belowTile.text]){
                        //make currentTile unoccupied and double value of belowTile
                        [currentTile setText:@""];
                        [unoccupied addObject:currentTile];
                        if ([belowTile.text isEqual:@"2"]){
                            [belowTile setText:@"4"];
                            score += 4;
                        } else if ([belowTile.text isEqual:@"4"]){
                            [belowTile setText:@"8"];
                            score += 8;
                        } else if ([belowTile.text isEqual:@"8"]){
                            [belowTile setText:@"16"];
                            score += 16;
                        } else if ([belowTile.text isEqual:@"16"]){
                            [belowTile setText:@"32"];
                            score += 32;
                        } else if ([belowTile.text isEqual:@"32"]){
                            [belowTile setText:@"64"];
                            score += 64;
                        }else if ([belowTile.text isEqual:@"64"]){
                            [belowTile setText:@"128"];
                            score += 128;
                        }else if ([belowTile.text isEqual:@"128"]){
                            [belowTile setText:@"256"];
                            score += 256;
                        }else if ([belowTile.text isEqual:@"256"]){
                            [belowTile setText:@"512"];
                            score += 512;
                        }else if ([belowTile.text isEqual:@"512"]){
                            [belowTile setText:@"1024"];
                            score += 1024;
                        }else if ([belowTile.text isEqual:@"1024"]){
                            [belowTile setText:@"2048"];
                            score += 2048;//TODO: Make it so that you win when you get a 2048 tile!
                        }
                        [scoreLabel setText:[@(score) stringValue]];
                    }
                }
            }
        }
    }
    
    //Call generateTile if the board isn't full
    if ([unoccupied count] > 0) {
        [self generateTile];
    }
    else {
        //Check if any more merges are possible. If not, game over!
        if (![self mergesPossible]){
            //Do game over stuff
            [gameOverLabel setText:@"Game over!"];
        }
    }
}

//BUG: grid[1][2] doesn't slide left
- (IBAction) slideLeft {
    NSLog(@"In slideLeft");
    //Move all occupied tiles left as far as they'll go
    //  Starting from top left corner and going down, then to next column and so on:
    //  if tile is occupied: check if tile left of it is unoccupied. if it is, move it left to that tile. then check the tile left of that one and so on.
    for (int i = 1; i < ROW_SIZE; i++){//start at 2nd row because you'll never move the top row by sliding up
        for (int j = 0; j < COL_SIZE; j++){
            //i is column (x coord), j is row (y coord)
            UILabel* currentTile = grid[j][i];
            if (![currentTile.text isEqual:@""]) {//If occupied
                //Want to move current tile left as many times as possible before hitting an occupied tile or the left side
                
                int k;//will become the leftest unoccupied reachable tile
                for (k=i-1; k > -1; k--){
                    UILabel* leftTile = grid[j][k];
                    if (![leftTile.text isEqual:@""]){
                        break;
                    }
                }
                k++;
                NSLog(@"i=%@ j=%@ k=%@\n",@(i),@(j),@(k));
                NSLog(@"Current tile has value %@\n",currentTile.text);
                if (i != k){//if the tile will actually move
                    UILabel* tileToSet = grid[j][k];
                    [tileToSet setText:currentTile.text];
                    [currentTile setText:@""];
                    [unoccupied removeObject:tileToSet];
                    [unoccupied addObject:currentTile];
                }
                //If the tile now above the current one is equal, merge them upward
                if (k > 0){
                    UILabel* currentTile = grid[j][k];
                    UILabel* leftTile = grid[j][k-1];
                    if ([currentTile.text isEqual:leftTile.text]){
                        //make currentTile unoccupied and double value of leftTile
                        [currentTile setText:@""];
                        [unoccupied addObject:currentTile];
                        if ([leftTile.text isEqual:@"2"]){
                            [leftTile setText:@"4"];
                            score += 4;
                        } else if ([leftTile.text isEqual:@"4"]){
                            [leftTile setText:@"8"];
                            score += 8;
                        } else if ([leftTile.text isEqual:@"8"]){
                            [leftTile setText:@"16"];
                            score += 16;
                        } else if ([leftTile.text isEqual:@"16"]){
                            [leftTile setText:@"32"];
                            score += 32;
                        } else if ([leftTile.text isEqual:@"32"]){
                            [leftTile setText:@"64"];
                            score += 64;
                        }else if ([leftTile.text isEqual:@"64"]){
                            [leftTile setText:@"128"];
                            score += 128;
                        }else if ([leftTile.text isEqual:@"128"]){
                            [leftTile setText:@"256"];
                            score += 256;
                        }else if ([leftTile.text isEqual:@"256"]){
                            [leftTile setText:@"512"];
                            score += 512;
                        }else if ([leftTile.text isEqual:@"512"]){
                            [leftTile setText:@"1024"];
                            score += 1024;
                        }else if ([leftTile.text isEqual:@"1024"]){
                            [leftTile setText:@"2048"];
                            score += 2048;//TODO: Make it so that you win when you get a 2048 tile!
                        }
                        [scoreLabel setText:[@(score) stringValue]];
                    }
                }
            }
        }
    }
    
    //Call generateTile if the board isn't full
    if ([unoccupied count] > 0) {
        [self generateTile];
    }
    else {
        //Check if any more merges are possible. If not, game over!
        if (![self mergesPossible]){
            //Do game over stuff
            [gameOverLabel setText:@"Game over!"];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
