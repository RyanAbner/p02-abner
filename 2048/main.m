//
//  main.m
//  2048
//
//  Created by Admin on 2/2/17.
//  Copyright © 2017 RyanAbner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
